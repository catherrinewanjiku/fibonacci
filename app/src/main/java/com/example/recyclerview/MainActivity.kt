package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    lateinit var tvname:RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun displayNames(){
        tvname = findViewById(R.id.tvname)
        var namesList = listOf("Anna","Beth","Charlie","Denise","Edgar","Cathy","Shanice","Mercy","Allo","Dylan","Elvis","IanJoe","Hallo","Wanjiku")
        var namesAdapter = namervadapter(namesList)
        tvname.layoutManager = LinearLayoutManager(baseContext)
        tvname.adapter = namesAdapter
    }
}